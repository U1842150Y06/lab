

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber22 {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
         
         // Exercise 2
		
		int guess = reader.nextInt(); //Read the user input
		while ((guess != number)&& (guess != -1)) {
				System.out.println("sorry");
                System.out.print("type -1 to quit or guess another:");
                 guess=reader.nextInt();
           }


	if (guess == number) {
				System.out.println("Congratulations");

		} else {

			    System.out.println("Sorry, the number was" + number);
         }

		
		reader.close(); //Close the resource before exiting
	}  
}

 
