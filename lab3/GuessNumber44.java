

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber44 {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
       
       int counter =1;
         
         // Exercise 4
		
		int guess = reader.nextInt(); //Read the user input
		while ((guess != number)&& (guess != -1)) {
				System.out.println("sorry");
                if (number>guess){
                       System.out.println("Mine is greater than your guess,");
        
        } else { 
               System.out.print("Type -1 to quit or guess another:");
        guess=reader.nextInt();
         counter++;
     }

	if (guess == number) {
				System.out.println("Congratulations you won after" + counter + "attempts" );

		} else {

			    System.out.println("Sorry, the number was" + number);
         }

		
		reader.close(); //Close the resource before exiting
	     }
	}  
}

 
